# Instant Message Notifier

## What does it do?

It checks your current IM messages via several CLI clients for different
platform and notifies you about potential messages.

Currently supported:
- Telegram via [`telegram-cli`](https://github.com/vysheng/tg)

Future work (WIP, currently not implemented):
- Matrix via [`matrix-commander`](https://github.com/8go/matrix-commander)
- Signal via [`signal-cli`](https://github.com/AsamK/signal-cli)

## Why?

Many IM clients, such as the Telegram Desktop client or the Signal Desktop
client are somewhat resource intensive, particularly on the CPU, leading to
high(er) power consumption. With this lightweight notifier script, you are not
required to have the desktop client running in the background at all times.

## Are there system requirements?

Yes.

### Common

Sending Notifications: `gdbus` (likely installed already)

### Telegram

Checking Telegram Messages: [`telegram-cli`](https://github.com/vysheng/tg)
```
# Debian
apt install telegram-cli

# PostmarketOS
apk add tg
```

Set it up via commandline:
```
telegram-cli
```

## Install IM Notifier

Pull this source directory, and run `debuild`:
```
git pull https://src.jayvii.de/pub/notifier.git
cd Notifier
debuild
```

This will create a Debian installation package `.deb`, which you may install
via:
```
sudo apt install ../im-notifier_*.deb
```

Packages are being built automatically by Gitlab-CI:
https://gitlab.com/jayvii_de/pp-notifier/-/jobs

You can Download the "artifacts", which contain the latest `.deb` version, that
you can also install via `apt`, just like above.

## Use IM Notifier

Installing IM-Notifier also creates an App-Icon in your application menu.
Opening this application will run the daemon in the background. You can start
this notifier automatically upon login:
```
mkdir -p ~/.config/autostart
cp /usr/share/applications/im-notifier.desktop ~/.config/autostart/
```

IM Notifier reads configurations from `~/.config/im-notifier/config`. A sample
file can be found in the root repository as [`config.sample`](./config.sample)

# Authoring & Maintenance

- JayVii <jayvii [AT] posteo [DOT] de>

# Contribute

You may contribute by sending patches to one of the authors or maintainers above.
